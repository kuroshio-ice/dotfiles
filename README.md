# README

## Setup

```
git clone https://gitlab.com/kuroshio-ice/dotfiles.git
cd dotfiles/
./setup.sh
```
### Vimrc

```
mkdir -p ~/.vim/autoload
mkdir -p ~/.vim/colors
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
git clone https://github.com/tomasr/molokai
mv molokai/colors/molokai.vim ~/.vim/colors/
```
